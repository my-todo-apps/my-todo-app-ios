//
//  ContentView.swift
//  My Todo App - ios
//
//  Created by Ng Han Shen on 17/11/2020.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
